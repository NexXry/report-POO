#include <iostream>
//#include <synchapi.h>
#include "point.hpp"

using namespace std;

void display(point p);


void point::affiche() {
    cout << "X : " << X << '\n';
    cout << "Y : " << Y  << '\n';
}

int main() {
    point p;
    point p2;
    p.intialise(1,3);
    p.deplace();
    display(p);
    p2.intialise(2,2);
    p.distant(p2);

    return 0;
}


void point::intialise(double x,double y) {
    X = x;
    Y = y;
}

void point::distant(point p2) {
    cout << "Distance :  X : " <<   p2.X - X << '\n';
    cout << "Distance :  Y : " <<  Y - p2.Y << '\n';
}

void point::deplace() {
    for (int i = 0; i < 10 ; ++i) {
        X -= 0.1;
        Y += 0.1;
        affiche();
        //Sleep(600);
    }
}

void display(point p){
    p.affiche();
}