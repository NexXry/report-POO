using namespace std;

class point {
private:
    static int nb_call_construct;
    static int nb_call_constructParam;
    static int nb_call_constructCopie;
    static int nb_call_destruct;
    double X;
    double Y;
    string nom;

public:
    point() {
        setNbdefaut(1);
        intialise(0,0,"");
    }

    point( double y,string nom) {
        setNbParam(1);
        intialise(0,y,nom);
    }

    point(double x, double y,string nom) {
        setNbParam(1);
        intialise(x,y,nom);
    }

    point(const point &p) {
        setNbCopie(1);
        X=p.X;
        Y=p.Y;
        nom=p.nom;
    }

    ~point()
    {
        setNbDes(1);
        cout<<"Destructeur : " << nom <<endl;
    }

    void getNbCount(){
        cout << "Defaut : " << nb_call_construct << '\n';
        cout << "Param : " << nb_call_constructParam << '\n';
        cout << "Copie : " << nb_call_constructCopie << '\n';
        cout << "Destruction : " << nom  << '\n';
    }

    void setNbdefaut(int nb){
        nb_call_construct+=nb;
    }
    void setNbParam(int nb){
        nb_call_constructParam+=nb;
    }
    void setNbCopie(int nb){
        nb_call_constructCopie+=nb;
    }
    void setNbDes(int nb){
        nb_call_destruct+=nb;
    }

    void intialise(double x, double y,string nom);
    void deplace();
    void affiche();
    void distant(point p2);
    void count();
};

