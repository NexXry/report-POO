#include <iostream>
//#include <synchapi.h>
#include "point.hpp"

using namespace std;

void display(point p);



void point::affiche() {
    cout << "X : " << X << '\n';
    cout << "Y : " << Y  << '\n';
    cout << "Nom : " << nom  << '\n';
}



int main() {
    point p(2,2,"p1");
    point p2(3,5,"p2");
    point p3(0,"p3");//construit avec le conctructeur
    point p4 = p;//construit avec le conctructeur par copie
    point p5(p);//construit avec le conctructeur par copie

    display(p);
    display(p2);
    display(p3);// donnée du constructeur
    display(p4);// donnée du constructeur par copie

    p.getNbCount();

    return 0;
}


void point::intialise(double x,double y,string p) {
    X = x;
    Y = y;
    nom = p;
}

void point::distant(point p2) {
    cout << "Distance :  X : " <<   p2.X - X << '\n';
    cout << "Distance :  Y : " <<  Y - p2.Y << '\n';
}

void point::deplace() {
    for (int i = 0; i < 10 ; ++i) {
        X -= 0.1;
        Y += 0.1;
        affiche();
        //Sleep(600);
    }
}

void display(point p){
    p.affiche();
}