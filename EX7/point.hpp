using namespace std;

class point {
private:
    double X;
    double Y;
    string nom;
public:
    point() {
        intialise(0,0,"");
    }

    point( double y,string nom) {
        intialise(0,y,nom);
    }

    point(double x, double y,string nom) {
        intialise(x,y,nom);
    }

    point(const point &p) {
        X=p.X;
        Y=p.Y;
        nom=p.nom;
    }

    ~point()
    {
        cout<<"Destructeur : " << nom <<endl;
    }

    void intialise(double x, double y,string nom);
    void deplace();
    void affiche();
    void distant(point p2);

};

