#include <iostream>

using namespace std;

void echange1(int,int);
int echange2(int *, int *);
void echange3(int &,int &);

int main() {
    int n=10,p=20;

    cout<<"avant appel :"<< n <<"\t"<< p << "\n";
    echange1(n,p); //n'interverti pas les argument
    cout<<"apres 1 appel :"<< n <<"\t"<< p << "\n";
    echange2(&n,&p);// interverti pas les argument
    cout<<"apres 2 appel :"<< n <<"\t"<< p << "\n";
    echange3(n,p); //interverti pas les argument
    cout<<"apres 3 appel :"<< n <<"\t"<< p << "\n";

    return 0;
}

void echange1(int a,int b){
    int c;
    c=a; a=b ; b=c;
}

int echange2(int *a, int *b){
    int c;
    c=*a; *a=*b ; *b=c;
}

void echange3(int & a,int & b){
    int  c;
    c=a; a=b ; b=c;
}
