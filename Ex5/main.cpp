#include <iostream>
//#include <synchapi.h>
#include "point.hpp"

using namespace std;

void display(point p);


void point::affiche() {
    cout << "X : " << X << '\n';
    cout << "Y : " << Y  << '\n';
}

int main() {
    point p;
    p.intialise();
    p.deplace();
    display(p);

    return 0;
}


void point::intialise() {
    X = 5.445;
    Y = 5.942;
}

void point::deplace() {
    for (int i = 0; i < 10 ; ++i) {
        X -= 0.1;
        Y -= 0.1;
        affiche();
        //Sleep(600);
    }
}

void display(point p){
    p.affiche();
}