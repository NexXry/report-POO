#include <iostream>
#include "vecteur3d.hpp"
using namespace std;

int main(){

    Vecteur3D A(1.2,2.3,3.4);
    Vecteur3D v(8,8,8);
    Vecteur3D b(8,8,8);
    A.affiche();
    A.fixer_abscisse(6);
    A.fixer_ordonnee(6);
    A.fixer_cote(6);
    cout << "abscisse : " << A.abscisse() << endl;
    cout << "ordonnee : " << A.ordonnee() << endl;
    cout << "cote : " << A.cote() << endl;
    cout << "Resultat (1 ou 0) : " << coincide(v,b) << endl; // coincide peut être appeler sans passer par un objet du fait quelle soit "Amie de la classe"

    return 0;
}