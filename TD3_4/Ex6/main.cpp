#include <iostream>

using namespace std;

int main() {

    int *p;
    const int *q;
    int* const r = new int(54);

    p = new int(5); // pointeur sur adresse mémoire de l'entier
    q = new int(6); // Const pointeur sur adresse mémoire de l'entier
    int *v = new int(8);
    int& s = (int&) v;
    const int& t = (int&) v;  // reférence adresse mémoire


    cout <<" p " << p <<endl;
    cout <<" q " << q <<endl;
    cout <<" r " << r <<endl;
    cout <<" v " << v <<endl;
    cout <<" s " << s <<endl;
    cout <<" t " << t <<endl;
    int *d;
    int *tab[10];
    for (int i = 0; i <  9 ; ++i) {
        int j = 1;

        d= new int(j);
        j=j*2-3;
        tab[i]=d;
        cout <<" tab I " << tab[i] <<endl;
    }


    delete p;
    delete q;
    delete r;
    delete [] *tab;
    delete d;


    return 0;
}

