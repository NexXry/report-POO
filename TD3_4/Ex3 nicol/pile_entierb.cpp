//
// Created by louis on 08/02/2022.
//

#include "pile_entier.hpp"
#include <iostream>
using namespace std;


// CONSTRUCTEURS

pile_entier:: pile_entier(){
    taille=20;
    tab = new int[taille];
    sommet=-1;// lorque une pile est vide, son sommet est defini à -1.
}

pile_entier:: pile_entier(int n){
    taille=n;
    sommet = -1;
    tab = new int[taille-1];
}

pile_entier::pile_entier(pile_entier const &pile){
    taille = pile.taille;
    sommet = pile.sommet;
    // tab = pile.tab; [FAUX] Ici on va créer et instancier un objet qui va pointer vers les mêmes données que l autre tableau. Lors de la destrcution , la premiuere destruction va fonctionner mais al deuxieme va nous donner un segmentation fault.
    // Il faut eviter le couplage de mémoire et pour eviter cela nous devons faire :
    this->tab = new int[taille];
    for(int i=0; i<taille; i++){
        tab[i] = pile.tab[i]; // on aurait pu prendre seulement sommet mais vu que l on est paresseux on prefere prendre taille car sinon on a de socnditions supplémrentaires si sommet = -1... On prefere ainsi faire la copie de tout
    }
}

// DESTRUCTEUR

pile_entier::~pile_entier(){
    delete [] tab;
    cout << "destruction de la pile" << endl;
}


// EMPILER

void pile_entier::empile(int p){
    if(pleine()!=1){
        sommet++;
        tab[sommet-1] = p;
    }
}

int  pile_entier::pleine() {
    int est_pleine = 1;
    if(sommet!=taille){
        est_pleine = 0;
    }
    return est_pleine;
}

int pile_entier::vide() {
    int est_vide = 0;
    if(sommet == -1){ // SI notre pile est vide
        est_vide = 1;
    }
    return est_vide;
}

// AFFICHER
void pile_entier::affiche() {
    cout << "Affichage de la pile :\n" << " Sa taille est de " << taille << " son sommet est de " << sommet
         << ".\n Elle est composee de :" << endl;
    for (int i = 0; i < sommet; ++i) {
        cout << "   [" << i << "] = " << tab[i] << endl; // faire methode pour afficher seulement le somet car on est sencé récupérer seulement le sommets
    }
}

    //faire depile
int pile_entier::depile(){
    int derniere_valeur = tab[sommet-1];
    sommet--; // suelement besoin de
    return derniere_valeur;
}




