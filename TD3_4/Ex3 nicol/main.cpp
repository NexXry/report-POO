#include <iostream>
#include "Pile_entier.hpp"

using namespace std;

int main() {
    Pile_entier pile1(2);
    Pile_entier pile2;
    Pile_entier pile3(pile1);

    pile1.empile(5);
    pile1.empile(1);
    pile1.depile();

    pile2.depile();

    pile1.affiche();


    cout << "Vide1 ? : " << pile1.vide() << '\n';
    cout << "Vide2 ? : " << pile2.vide() << '\n';
    cout << "Vide3 ? : " << pile3.vide() << '\n';
    return 0;
}

