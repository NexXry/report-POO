#include <iostream>
#include "Pile_entier.hpp"
using namespace std;


Pile_entier::Pile_entier(int n) {
    taille=n;
    array= new int[taille];
    for (int i = 0; i < taille ; ++i) {
        array[i]=-1;
    }
    sommet=-1;
}

Pile_entier::Pile_entier() {
    taille = 19;
    array= new int[taille];
    for (int i = 0; i < taille ; ++i) {
        array[i]=-1;
    }
    sommet=-1;
}

Pile_entier::Pile_entier(Pile_entier const &pile) {
    taille=pile.taille;
    array= pile.array;
    sommet=-1;
}

Pile_entier::~Pile_entier() {
    delete  [] array;
}

void Pile_entier::empile(int p) {
    if(pleine() == 0){
            sommet++;
            array[sommet]=p;
    } else{
        cout << "Vous ne pouvez pas emplier le tableau et plein";
    }

}

int Pile_entier::depile() {

    array[sommet]=-1;
    sommet--;
    return array[sommet];
}

int Pile_entier::pleine() {
    int i=0;
    int count=0;
    for (int j = 0; j < taille ; ++j) {
        if(array[j] != -1)
        {
            count++;
        }
    }

    if(count==taille){
        i=1;
    }

    return i;
}

int Pile_entier::vide() {
    int i=1;
    if(sommet!=-1){
        i=0;
    }
    return i;
}

void Pile_entier::affiche() {
    cout << "Taille du tableau : " << taille << endl;
    for (int i = 0; i < taille; ++i) {
        cout << "I :" << array[i] << "\n" ;
    }
}
