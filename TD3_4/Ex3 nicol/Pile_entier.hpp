//
// Created by nicolas castex on 08/02/2022.
//
using namespace std;

#ifndef UNTITLED_PILE_ENTIER_HPP
#define UNTITLED_PILE_ENTIER_HPP

class Pile_entier {
public:
    int *array;
    int taille;
    int sommet;

    Pile_entier();
    Pile_entier(int n);
    Pile_entier (Pile_entier const &pile);
    void empile (int p);
    int depile ();
    int pleine ();
    int vide ();
    void affiche ();
    ~Pile_entier();

};



#endif //UNTITLED_PILE_ENTIER_HPP
