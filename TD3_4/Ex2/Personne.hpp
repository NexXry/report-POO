//
// Created by louis on 01/02/2022.
//

#ifndef TD3_4_PERSONNE_H
#define TD3_4_PERSONNE_H
class Personne {
private:
    char* nom;
    char* prenom;
    int age;

public:
    char *getNom() const;

    void setNom(char *nom);

    char *getPrenom() const;

    void setPrenom(char *prenom);

    int getAge() const;

    void setAge(int age);

    Personne(); // Constructeur vide.
    Personne(char* nom, char* prenom, int age);// Constructeurs avec ensemble des parametres
    Personne(const Personne &per); // contructeur de copie
    void affiche() const; // fonction affiche
    virtual ~Personne(); // permet la destruction de l'objet.
};




#endif //TD3_4_PERSONNE_H
