//
// Created by louis on 01/02/2022.
//
#include "Personne.hpp"

int main(){
    char* nom = "DAVAINE";
    char* prenom = "Louis-Amand";
    int age = 17;

    Personne personne1 = Personne();
    personne1.affiche();
    Personne personne2 = Personne(nom, prenom, age);
    personne2.affiche();
    Personne personne3 = Personne(personne2);
    personne3.affiche();




return 0;
}