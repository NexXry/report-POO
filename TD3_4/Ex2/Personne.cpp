
#include <iostream>
#include "Personne.hpp"

using namespace std;

    Personne::Personne() {
        }

    Personne::Personne(char* nom, char* prenom, int age) {

        // nom
            this->nom = nom;
            this->prenom = prenom;
            this->age = age;
        }

        Personne::Personne(const Personne &per) {
            this->nom = nom;
            this->prenom = prenom;
            this->age = age;
        }



void Personne::affiche() const{
        cout << "Cette personne s'appelle "  << this->getNom() << endl <<  " " << this->getPrenom() << " et a " << this->getAge() << " ans." << endl;  ;
}

char *Personne::getNom() const {
    return nom;
}

void Personne::setNom(char *nom) {
    Personne::nom = nom;
}

char *Personne::getPrenom() const {
    return prenom;
}

void Personne::setPrenom(char *prenom) {
    Personne::prenom = prenom;
}

int Personne::getAge() const {
    return age;
}

void Personne::setAge(int age) {
    Personne::age = age;
}

Personne::~Personne() {
}
