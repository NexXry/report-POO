#include <iostream>
#include "vecteur3d.hpp"
using namespace std;

int main(){

    Vecteur3D A(1.2,2.3,3.4);
    Vecteur3D v(8,8,8);
    A.affiche();
    A.fixer_abscisse(8);
    A.fixer_ordonnee(8);
    A.fixer_cote(8);
    cout << A.abscisse() << endl;
    cout << A.ordonnee() << endl;
    cout << A.cote() << endl;
    cout << A.coincide(v) << endl;

    return 0;
}