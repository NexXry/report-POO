#include <iostream>
#include "vecteur3d.hpp"

using namespace std;

void Vecteur3D::affiche() {
    cout << "x: " << static_cast<double>(x) << endl;
    cout << "y: " << static_cast<double>(y) << endl;
    cout << "z: " << static_cast<double>(z) << endl;
}

void Vecteur3D::affiche(const char* string) {
    cout << "string" << endl;
    cout << "x: " << x << endl;
    cout << "y: " << y << endl;
    cout << "z: " << z << endl;
}

int Vecteur3D::abscisse() {
    return x;
}

int Vecteur3D::ordonnee() {
    return y;
}

int Vecteur3D::cote() {
    return z;
}

void Vecteur3D::fixer_abscisse(int nouvelle_abscisse) {
    this->x = nouvelle_abscisse;
}

void Vecteur3D::fixer_ordonnee(int nouvelle_ordonnee) {
    this->y = nouvelle_ordonnee;
}

void Vecteur3D::fixer_cote(int nouvelle_cote) {
    this->z = nouvelle_cote;
}

bool Vecteur3D::coincide(Vecteur3D v) {
    bool boolean = false;
    if (v.x == x && v.y == y && v.z == z){
        boolean = true;
    }
    return boolean;
}
