class Vecteur3D {
    private:

    double x;
    double y;
    double z;

    public:
    void affiche();
    void affiche(const char* string);
    int abscisse();
    int ordonnee();
    int cote();
    void fixer_abscisse(int nouvelle_abscisse);
    void fixer_ordonnee(int nouvelle_ordonnee);
    void fixer_cote(int nouvelle_cote);
    bool coincide(Vecteur3D v);
    inline Vecteur3D (){x=0;y=0;z=0;};
    inline Vecteur3D(double x,double y,double z){ this->x=x; this->y=y; this->z=z; };
};