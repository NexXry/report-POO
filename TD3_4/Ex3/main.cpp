//
// Created by louis on 15/02/2022.
//

#include "pile_entier.hpp"
#include <iostream>

using namespace std;
int main(){
    pile_entier pile0(5);
    pile0.empile(77);
    pile0.empile(38);
    pile0.empile(38);
    pile0.empile(38);
    pile0.empile(37);
    pile0.affiche();
    cout << pile0.depile() <<endl;
    pile0.affiche();
    pile0.empile(55);
    pile0.affiche();
}