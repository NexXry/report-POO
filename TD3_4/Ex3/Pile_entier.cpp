//
// Created by louis on 08/02/2022.
//

#include "pile_entier.hpp"
#include <iostream>
using namespace std;


// CONSTRUCTEURS

pile_entier:: pile_entier(){
    taille=20;
    tab = new int[taille];
    sommet=-1;// lorque une pile est vide, son sommet est defini à -1.
}

pile_entier:: pile_entier(int n){
    taille=n;
    sommet = -1;
    tab = new int[taille-1];
}

pile_entier::pile_entier(pile_entier const &pile){
    taille = pile.taille;
    sommet = pile.sommet;

    this->tab = new int[taille]; // Allocation de mémoire pour ne pas écraser la mémoire existante.
    for(int i=0; i<taille; i++){
        tab[i] = pile.tab[i];
    }
}

// DESTRUCTEUR

pile_entier::~pile_entier(){
    delete [] tab;
    cout << "destruction de la pile" << endl;
}


// EMPILER

void pile_entier::empile(int p){
    if(pleine()!=1){
        if(sommet == -1){sommet=0;} // si première insertion dans la pile alors sommet = 0
        tab[sommet] = p;
        sommet++;
    }
}

int  pile_entier::pleine() {
    int est_pleine = 1;
    if(sommet!=taille){
        est_pleine = 0;
    }
    return est_pleine;
}

int pile_entier::vide() {
    int est_vide = 0;
    if(sommet == -1){
        est_vide = 1;
    }
    return est_vide;
}

// AFFICHER
void pile_entier::affiche() {
    cout << "Affichage de la pile :\n" << " Sa taille est de " << taille << " son sommet est de " << sommet
         << ".\n Elle est composee de :" << endl;
    for (int i = 0; i < sommet; ++i) {
        cout << "   [" << i << "] = " << tab[i] << endl;
    }
}

    //faire depile
int pile_entier::depile(){
    int derniere_valeur = tab[sommet-1];
    sommet--;
    return derniere_valeur;
}




