//
// Created by louis on 08/02/2022.
//

#ifndef TD3_4_PILE_ENTIER_H
#define TD3_4_PILE_ENTIER_H


class pile_entier {
private :
    int taille;
    int sommet;
    int *tab;
public:
    pile_entier(int n);
    pile_entier();
    pile_entier(pile_entier const &pile);
    ~pile_entier();
    void empile(int p);
    int depile();
    int pleine();
    int vide();
    void affiche();
};


#endif //TD3_4_PILE_ENTIER_H

