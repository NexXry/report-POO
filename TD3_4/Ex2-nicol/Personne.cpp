#include <iostream>
#include <cstring>
#include "Personne.hpp"

using namespace  std;

Personne::Personne() {
    nom = new char[MAX];
    strcpy(nom, " ");
    prenom = new char[MAX];
    strcpy(prenom, " ");
    age = 0;
}


Personne::Personne(char* nomp, char* prenomp, int agep)  {
    int l;
    l = strlen(nomp); // quantite de char dans le nom sans le \0
    l = l + 1;        // +1 pour le \0
    nom = new char[l];
    strcpy(nom, nomp);
    l = strlen(prenomp); // quantite de char dans le nom sans le \0
    l = l + 1;        // +1 pour le \0
    prenom = new char[l];
    strcpy(prenom, prenomp);
    age = agep;
}

Personne::Personne(const Personne &personne)  {
    int n;
    n = strlen(personne.nom); // quantite de char dans le nom sans le \0
    n = n + 1;        // +1 pour le \0
    nom = new char[n];
    strcpy(nom, personne.nom);
    int o;
    o = strlen(personne.prenom);
    o = o + 1;
    prenom = new char[o];
    strcpy(prenom, personne.prenom);
    age = personne.age;
}

void Personne::affiche() const{
    cout << "Cette personne s'appelle "  << this->nom << endl <<  " " << this->prenom << " et a " << this->age << " ans." << endl;  ;
}
Personne::~Personne() {
    delete []nom;
    delete []prenom;
}