#include <iostream>
#include <cstring>
#include "Personne.hpp"
using namespace std;

int main() {
    int age = 20;
    Personne p("CASTEX","NICOLAS",age);
    Personne p2;
    Personne p3(p);

    p.affiche();
    p2.affiche();
    p3.affiche();
    return 0;
}

