#include <iostream>
#include "vecteur3d.hpp"

using namespace std;

void Vecteur3D::affiche() {
    cout << "x: " << static_cast<double>(x) << endl;
    cout << "y: " << static_cast<double>(y) << endl;
    cout << "z: " << static_cast<double>(z) << endl;
}

void Vecteur3D::affiche(const char* string) {
    cout << string << endl;
    cout << "x: " << x << endl;
    cout << "y: " << y << endl;
    cout << "z: " << z << endl;
}

