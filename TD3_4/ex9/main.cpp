#include <iostream>
#include "vecteur3d.hpp"
using namespace std;

int main(){

    Vecteur3D A(1.2,2.3,3.4);
    Vecteur3D v(8,8,8);
    A.affiche(); // affichage du vecteur
    v.affiche("Vector"); // surcharge de la fonction affiche avec param char*


    return 0;
}