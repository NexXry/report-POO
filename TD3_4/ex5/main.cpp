#include <iostream>
#include "Personne.hpp"
using namespace std;

void affiche(const int& n){
    // n=8; la modification de l'entier passer en paramètre de la fonction n'est pas possible car il est une constante (const int& n)
    cout << "int n : " << n << endl;
}

int main() {
    int age = 20;
    char* prenom = "nicolas";
    char * nom = "castex" ;

    Personne p(nom,prenom,age);
    Personne p2;
    Personne p3(p);

    p.affiche();
    p2.affiche();
    p3.affiche();
    affiche(5);
    return 0;
}

