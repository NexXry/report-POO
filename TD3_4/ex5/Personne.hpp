using namespace std;
#define MAX 20
class Personne {
private:
    char* nom;
    char* prenom;
    int age;

public:
    Personne();
    Personne(char* nomp, char* prenomp, int agep);
    Personne(const Personne &personne);

    void affiche() const;

    virtual ~Personne(); // destructeur de la classe personne

};

