#include <iostream>
using namespace std;

inline void copie2(int source, int dest) { source=dest ; cout << "copie2 : " << source << endl; } // fonction en ligne copie d'entier
inline void copie2(double source, double dest) { source=dest ; cout << "copie2 double : " << source << endl;
    if (source == 0){
        return;
    } else {
        copie2(source,dest/2);
    }
} // fonction en ligne copie de réel

#define copie1(source,dest) source=dest ; // directive qui définie un fonction de copie (multi-type)

int main() {
    int y,x;
    double w,z;
    y = copie1(x,8); // La directive à besoin qu'on lui passe une variable en paramètre pour fonctionner / appel avec un entier
    cout << "copie1 : " << y << endl;
    copie2(1,8); // appel de la fonction de copie entier
    z = copie1(w,8.99); // appel de la directive avec un réel.
    cout << "copie1 double : " << z << endl;
    copie2(1.25,8.69); // appel de la fonction de copie réel

    return 0;
}

