//
// Created by louis on 22/02/2022.
//

#include "Somme.hpp"
#include <iostream>
#include <stdio.h>

using namespace std;


// Fonction somme avec deux paramètres de type int.
int Somme::somme(int x, int y){
    return x+y;
}
// Fonction somme avec deux paramètres de type float.
float Somme::somme(float x, float y){
    return x+y;
}

// fonction somme avec deux paramètres de type tableau d'entiers de taille 10.
void Somme::somme(int x[MAX_SIZE_TAB], int y[MAX_SIZE_TAB]){
    static int resultat[MAX_SIZE_TAB];
    for (int i=0;i<MAX_SIZE_TAB;i++){
        resultat[i] = x[i] + y[i];
        cout << resultat[i] << endl;
    }
}

int * Somme::somme1(int * x, int * y){
    static int res[MAX_SIZE_TAB];
    for (int i=0;i<MAX_SIZE_TAB;i++){
        res[i] = x[i] + y[i];
        cout << res[i] << endl;
    }
    return res;
}

// Fonction somme avec  trois paramètres de même type (int)
int Somme::somme(int x, int y, int z){
    return x+y+z;
}

float Somme::somme(int x, float y) {
    return static_cast<float>(x)+y;
}

int main(){
    Somme s;
    cout << s.somme(2,3) << endl;

    cout << s.somme (10, 40, 50) << endl;


    float a = 3.77;
    float b = 4.4;
    cout << s.somme(a,b) << endl; //a tester car lorsque l on met les valeurs en dur dans la fonction ca ne fonctionne pas

    int tab1[10] = {1,2,3,4,5,6,7,8,9,10};
    int tab2[10] = {1,2,3,4,5,6,7,8,9,10};

    s.somme(tab1, tab2);
    int * resF = NULL;
    resF = s.somme1(tab1,tab2 );

    cout << "test  pour voir si le tableau a bien fonctionné" << resF[1] <<endl;
    int x = 5;
    float y = 6.8;
    cout <<"TEST int et float :" << s.somme(x,y ) << endl;
    return 0;


}


