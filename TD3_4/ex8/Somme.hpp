//
// Created by louis on 22/02/2022.
//

#ifndef TD3_4_SOMME_HPP
#define TD3_4_SOMME_HPP
const int MAX_SIZE_TAB =10;

class Somme {

public:
    int somme(int x, int y );
    float somme(float x, float y);
    void somme(int x[MAX_SIZE_TAB], int y[MAX_SIZE_TAB]);
    int * somme1(int * x, int * y);
    int somme(int x, int y, int z);
    float somme(int x, float y);
};


#endif //TD3_4_SOMME_HPP
