
#include <iostream>
#include "Personne.hpp"

using namespace std;

    Personne::Personne() {
        nom[0] = '\0';
        prenom[0] = '\0';
        age = 0;
    }

    Personne::Personne(char *nom, char *prenom, int age) {
        int i = 0;
        // nom
        while (nom[i] != '\0' && i < sizeof(this->nom)) {
            this->nom[i] = nom[i];
            i++;
        }
        i = 0;
        // prenom
        while (prenom[i] != '\0' && i < sizeof(this->prenom)) {
            this->prenom[i] = prenom[i];
            i++;
        }
        // age
        this->age = age;
    }

        Personne::Personne(const Personne &per) {
        int i =0;
        while (per.nom[i] != '\0' && i < sizeof(this->nom)) {
            this->nom[i] = per.nom[i];
            i++;
        }
        i=0;
        while (per.prenom[i] != '\0' && i < sizeof(this->prenom)) {
            this->prenom[i] = per.prenom[i];
            i++;
        }
        this->age = per.age;
    }



void Personne::affiche() const{
        cout << "Cette personne s'appelle "  << this->prenom << endl <<  " " << this->nom << " et a " << this->age << " ans." << endl;  ;
}
