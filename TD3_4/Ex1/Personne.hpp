//
// Created by louis on 01/02/2022.
//

#ifndef TD3_4_PERSONNE_H
#define TD3_4_PERSONNE_H
const int MAX_CHAR = 20;
class Personne {
private:
    char nom[MAX_CHAR];
    char prenom[MAX_CHAR];
    int age;

public:

    Personne(); // Constructeur vide.
    Personne(char nom[MAX_CHAR], char prenom[MAX_CHAR], int age);// Constructeurs avec ensemble des parametres
    Personne(const Personne &per); // contructeur de copie
    void affiche() const; // fonction affiche

};




#endif //TD3_4_PERSONNE_H
