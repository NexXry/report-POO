//
// Created by louis on 01/02/2022.
//
#include "Personne.hpp"

int main(){
    char nom[MAX_CHAR] = "DAVAINE";
    char prenom[MAX_CHAR] = "Louis-Amand";
    int age = 17;

    Personne personne1 = Personne(); // Appel du constructeur sans paramètre
    Personne personne2 = Personne(nom, prenom, age); // Appel constructeur avec paramètre
    Personne personne3 = Personne(personne2); // Appel du constructeur par copie
    personne1.affiche();
    personne2.affiche();
    personne3.affiche();
    return 0;
}