#include "Personne.hpp"
using namespace std;

int main() {
    char nom[20] = "Castex";
    char prenom[20] = "Nicolas";
    int age = 20;

    Personne personneTab[3];

    Personne p(nom,prenom,age);
    Personne p2;
    Personne p3(p);

    personneTab[0]=p;
    personneTab[1]=p2;
    personneTab[2]=p3;


    p.affiche();
    p2.affiche();
    p3.affiche();



    return 0;
}

