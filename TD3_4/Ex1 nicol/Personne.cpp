#include <iostream>
#include "Personne.hpp"

using namespace  std;

Personne::Personne() {
}

Personne::Personne(char nomp[MAX_SIZE], char prenomp[MAX_SIZE], int agep)  {
    for (int i = 0; i < 19; ++i) {
        nom[i]=nomp[i];
        prenom[i] = prenomp[i];
    }
    age = agep;
}

Personne::Personne(const Personne &personne)  {
    for (int i = 0; i < 19; ++i) {
        nom[i]=personne.nom[i];
        prenom[i] = personne.prenom[i];
    }
    age = personne.age;
}

void Personne::affiche() const {cout<< "P1 " << nom << " / " << prenom << " / " << age << "\n" ;}

Personne::~Personne() {
}
