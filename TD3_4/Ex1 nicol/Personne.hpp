using namespace std;
#define MAX_SIZE 20
class Personne {
public:
    char nom[MAX_SIZE];

    char prenom[MAX_SIZE];
    int age;

    Personne();
    Personne(char nomp[MAX_SIZE], char prenomp[MAX_SIZE], int agep);
    Personne(const Personne &personne);

    void affiche() const;

    virtual ~Personne();


};

