#include <iostream>

using namespace std;

void fct();
void fct(int, int=12);

int main() {
    int n = 10,p=20;
    fct(n,p);
    fct(n);
    fct();
    return 0;
}

void fct(int a, int b){
    cout << "Premier arg :" << a << "\t" <<"Second arg : "<<b<<endl;
}

void fct(){
    cout << "SANS PARAM"<<endl;
}
